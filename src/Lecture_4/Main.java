package Lecture_4;


import Lecture_4.model.Book;

public class Main {
    public static void main(String[] args) {
        Book [] books = new Book[2];

        //1
        Book voynaIMir = new Book();
        voynaIMir.author = "Tolstoy";
        voynaIMir.publishes = "Moskva";
        voynaIMir.year = 1856;
        books[0] = voynaIMir;

        //2
        Book annaKarenina = new Book();
        annaKarenina.author = "Tolstoy";
        annaKarenina.publishes = "Moskva";
        annaKarenina.year = 1877;
        books[1] = annaKarenina;

        for(Book s : books){
            System.out.println(s.getInfo());
        }
    }
}
